# Shopping

## 一、整体介绍
本项目是一个移动端的 `UI 项目`，项目所采用的框架为 `HarmonyOS` 的 `ArkUI`，
编程语言为 `ArkTS`，是 `Typescript` 的超集，也即 `Javascript` 的超超集，
代码编写遵循 `链式调用` 和 `函数式声明` 两大风范。视图页面以购物平台的 App 页面为蓝本，
通过忽视后台逻辑进行简化，只聚焦于 UI 布局和基本操作交互上，其效果图如下：

![alt 运行效果图](./doc/img.png)

本项目所使用的 SDK 版本如下所示：

![alt SDK](./doc/sdk.png)

因此，使用本项目 有几个前提必须达到：
- 1、安装有 DevEco Studio 的计算机；
- 2、对 Typescript 和 javascript 有一定的了解
- 3、对 HarmonyOS 的 API 有一定的了解
- 4、对 lamda 表达式比较熟悉，和对链式调用比较习惯
- 5、对移动端 UI 布局有所了解

## 二、目录说明
本项目的目录结构如下图所示：

![alt 项目目录结构](./doc/project.png)

想基于本项目进行改造的开发者，只需留意途中货色方框所圈选的部分。
AppScope 目录下主要放置应用 logo 和应用 label 相关资源文件和设置文件。
而 src 目录则是存放项目源码和应用内页使用到的图像等资源文件的地方。

src/main 目录具体由子目录 `ets` 和子目录 `resources`，以及配置文件 `module.json5`组成，
其中 ets 目录用于存放源代码文件，resource 目录用于存放资源文件。对 resource 目录下的
rawfile 目录，建议直接拷贝，里面是视图页面使用到的图片和 icon。

下面就 ets 目录进行重点说明：

### 1、ets/comp 目录
该目录的详情，如下图：

![alt comp目录详情](./doc/comp.png)

如上所示，该目录中文件来自对应用视图页面，即 page 的拆分。如此操作，是为了减少 page 文件的代码量，
和遵循`组件化`的开发思想。comp 目录下，一个文件就是一个 `Component`，而该 Component 最终由哪个
Page 进行使用，可以从文件名上进行获知，例如以 Detail 开头的几个 Component 同属于某个 Detail Page。

对于不想同我这样分散管理的，可以将它们重新整合到一个 ArkTS 文件中，只不过整合之后的文件会变得
非常臃肿，缺乏简洁美，降低代码可读性。

### 2、ets/data 目录
![alt data 目录](./doc/data.png)

data 目录在本 UI 项目中，具有 `数据源` 的身份地位，通过结构化的硬编码，为 Page 页面提供
展示内容，因此，该目录是改造时重点关注的，如果想实现通过网络加载数据并显式到应用页面上的效果，
那么该目录下的硬编码的数据就要彻底重构。

硬编码是采用的数据结构，是根据 `ets/model` 目录下的数据模型进行编写，
也即每个数据集对应一个数据 model，所以，将 data 目录改造，就必须对 model 也进行同样的改造。

### 3、ets/model 目录

![alt model目录](./doc/model.png)

model 目录下的情况如上所述，有四个数据 model 和一个进行数据装载的 LoadData 构成，
具体的数据结构，还请打开对应的文件进行阅读，这里不过多展开。

### 4、ets/pages 目录
![alt model目录](./doc/pages.png)

pages 目录下存放的就是用户所直接看到的应用视图页面，从上图可以看出，本 UI 项目一共设置了
5 个视图页面，但实际上能看到的只有 4 个页面，因为 Index 和 HomePage 实际上相同的。
4 个视图中，Index、ShopCart 和 MyInfo 通过一个底部导航栏进行切换，而 ShoppingDetail
则是在 Index 页面中进行跳转，具体的，就是用户点击 Index 页面的商品列表时就会进入商品详情页

``这些 Page 文件必须在 resources/base/profile/main_pages.json 文件中进行配置。``

### 5、ets/entryability 目录
该目录下的内容，实际上保持着 IDE 自动生成的内容，并未做过任何修改。

## 三、编码指导
想要通过手动编码的形式去使用本项目的，请按照下方的顺序：
- 1、整体复制 resource/rawfile 目录
- 2、不想修改数据源的，直接整体复制 ets/data 目录
- 3、编写 model 下的内容，以便了解本项目使用的数据结构
- 4、以逐 Page 的形式，编写 ets/comp 目录下的 Component，每编写完一个 Page 的 Component，
就去 ets/pages 下编写对应的 Page，并及时进行预览
- 5、Index.ets 文件放到最后进行编写

由于目前市面上没有其他 IDE 对 HarmonyOS 项目进行支持，所以，所有的编码都必须在 DevEco Studio
中完成。

## 四、使用说明
本项目的使用，其实就是对最终的视图页面效果进行查看。由于本项目并没有用到后端逻辑，
所以，本项目无需使用真机进行部署，直接用 DevEco Studio 的预览窗口便能完成检验。
检验的时候，用 Index.ets 文件进行预览，通过该 Page 可以将项目中所有的 Page 串联起来。

