/* Author: 彭友聪 
 * email：2923616405@qq.com 
 * date: 2024/4/11 10:28
 * file: GoodsDataModel.ets
 * product: DevEco Studio
 * */
import { ArsList } from '../data/ArsList';
import { GoodsComposition } from '../data/GoodsComposition';
import { MenuSet } from '../data/MenuSet';
import { MoreSet } from '../data/MoreSet';
import { TransSet } from '../data/TransSet';
import { ArsData } from './ArsData';

import { GoodsData } from './GoodsData';
import { ImageItem } from './ImageItem';
import { MyMenu } from './Menu';

export function getDefaultGoodsData(): GoodsData {
  /*
   * "title": 'HUAWEI nova 8 Pro ',
    "content": 'Goes on sale: 10:08',
    "price": '3999',
    "imgSrc": $rawfile('picture/HW (1).png')
   * */
  return new GoodsData('HUAWEI nova 8 Pro ',
    'Goes on sale: 10:08',
    3999,
    $rawfile('picture/HW (1).png'))
}

export function initializeOnStartup() : Array<GoodsData> {
  let goodsDataArray : Array<GoodsData> = [];
  GoodsComposition.forEach( item => {
    goodsDataArray.push(new GoodsData(item.title, item.content, item.price, item.imgSrc))
  })

  return goodsDataArray;
}

export function getIconPath() : Array<string> {
  let iconPath: Array<string> = [
    `nav/icon-buy.png`,
    `nav/icon-shopping-cart.png`,
    `nav/icon-my.png`
  ];

  return iconPath;
}

export function getIconPathSelect() : Array<string> {
  let iconPathSelect: Array<string> = [
    'nav/icon-home.png',
    'nav/icon-shopping-cart-select.png',
    'nav/icon-my-select.png'
  ];

  return iconPathSelect;
}

export function getDetailImages(): Array<string> {
  let detailImages: Array<string> = [
    'computer/computer1.png',
    'computer/computer2.png',
    'computer/computer3.png',
    'computer/computer4.png',
    'computer/computer5.png',
    'computer/computer7.png',
    'computer/computer8.png'
  ]

  return detailImages;
}

export function getMenu(): Array<MyMenu> {
  let menuArray: Array<MyMenu> = [];
  MenuSet.forEach(item => {
    menuArray.push(new MyMenu(item.title, item.num))
  })
  return menuArray;
}

export function getTrans(): Array<ImageItem> {
  let imageItems: Array<ImageItem> = [];
  TransSet.forEach(item => {
    imageItems.push(new ImageItem(item.title, item.imageSrc));
  })
  return imageItems;
}

export function getMore() : Array<ImageItem> {
  let imageItemArray: Array<ImageItem> = [];
  MoreSet.forEach(item => {
    imageItemArray.push(new ImageItem(item.title, item.imageSrc));
  })
  return imageItemArray;
}

export function getArs() : Array<ArsData> {
  let arsItemArray: Array<ArsData> = [];
  ArsList.forEach(item => {
    arsItemArray.push(new ArsData(item.title, item.content));
  })
  return arsItemArray;
}
